'use strict'

var positions = [
    'Телепорт бытовой VZHIH-101',
    'Отвертка ультразвуковая WHO-D',
    'Ховерборд Mattel 2016',
    'Нейтрализатор FLASH black edition',
    'Меч световой FORCE (синий луч)'
];

var prices = [
    10000,
    4800,
    9200,
    2500,
    57000
];

var hitName = positions[2],
    hitPrice = prices[2];

//Задача № 1

var hit = {};
hit.name = hitName;
hit.price = hitPrice;

console.log(`Хит продаж мартобря: <${hit.name}> цена ${hit.price} Q`);

//Задача № 2
console.log('\n');

var items = [];

for (var i = 0; i < positions.length; i++) {
    items[i] = {
        name: positions[i],
        price: prices[i]
    }

}

console.log(`Купите ${items[4].name} по цене ${items[4].price} Q`);

//Задача № 3
console.log('\n');

function showDiscount(product, quantity) {
    var discount = discountSize(quantity);
    var orderSumWithDiscount = product.price * quantity * discount;
    var orderDiscount = product.price * quantity * (1 - discount);
    console.log(`${product.name} — стоимость партии из ${quantity} штук ${orderSumWithDiscount} Q (скидка ${Math.round((1-discount)*100)} %), ваша выгода ${Math.round(orderDiscount)} Q! `);
}

function discountSize(quantity) {
    if (quantity < 10) {
        return 0.95;
    } else if (quantity >= 10 && quantity < 50) {
        return 0.93;
    } else if (quantity >= 50 && quantity < 100) {
        return 0.9;
    } else {
        return 0.85;
    }
}

showDiscount(items[0], 12);
showDiscount(items[3], 97);

//Задача № 4
console.log('\n');

items[3].amount = 4;

function updateAmount(product, amount = 1) {
    if (!('amount' in product)) {
        return;
    }
    if (product.amount === 0 || product.amount < amount) {
        console.log(`${product.name} закончился на складе`)
    } else if (product.amount > amount) {
        product.amount -= amount;
        console.log(`${product.name} — остаток ${product.amount} шт`);
    } else if (product.amount === amount) {
        product.amount -= amount;
        console.log(`Это был последний ${product.name}, вам повезло!`);
    } else {
        console.log('Что-то пошло не так:(');
    }
}

updateAmount(items[1], 17);
updateAmount(items[3], 3);
updateAmount(items[3]);